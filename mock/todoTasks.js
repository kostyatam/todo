'use strict'

export default [
    {
        task: 'make actions',
        isDone: false
    },
    {
        task: 'make stores',
        isDone: false
    },
    {
        task: 'create server',
        isDone: true
    }
]